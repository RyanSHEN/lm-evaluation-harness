
# ceval

# for LENGTH in 256
# do
#     for STEP in 45000
#     do
#         python main.py \
#             --model transnormer2 \
#             --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-hf/7B-$STEP,max_gen_token=$LENGTH,repetition_penalty=1.1,dtype="bfloat16" \
#             --tasks Ceval-valid-* \
#             --num_fewshot 5 \
#             --output_path results/transnomer-7b-$STEP-ceval-5shot-bf16-mt$LENGTH-rp1.1-xdosmaple.json \
#             --batch_size 4 \
#             --device cuda:0 \
#             2>&1 | tee logs/transnomer-7b-$STEP-ceval-5shot-bf16-mt$LENGTH-rp1.1-xdosmaple.log
#     done
# done

for STEP in 62000
do
    python main.py \
        --model transnormer \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-7b/hf/7B-$STEP,max_length=256,repetition_penalty=1.0,dtype="bfloat16" \
        --tasks Ceval-valid-* \
        --num_fewshot 5 \
        --output_path results/transnomer-7b-$STEP-ceval-5shot-bp16-len256-xdosmaple-bsAuto.json \
        --batch_size auto \
        --device cuda:1 \
        2>&1 | tee logs/transnomer-7b-$STEP-ceval-5shot-bp16-len256-xdosmaple-bsAuto.log
done