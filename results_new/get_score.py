import glob
import json

import numpy as np

for path in sorted(glob.glob('transnomer-7b*-hellaswag-*'),
                   key=lambda x: int(x.split('-')[2])):
    scores = []
    ppls = []
    with open(path, 'r') as f:
        jobj = json.load(f)
        for k, v in jobj['results'].items():
            scores.append(float(v['acc_norm']))
            # ppls.append(float(v['ppl']))

    # print(scores, len(scores))
    print(path, np.mean(scores) * 100)
