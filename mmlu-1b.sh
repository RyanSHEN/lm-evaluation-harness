# mmlu

for STEP in 380000
do
    python main.py \
        --model transnormer2 \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-1b/hf/1B-$STEP,max_length=256,repetition_penalty=1.1,dtype="bfloat16" \
        --tasks hendrycksTest-* \
        --num_fewshot 5 \
        --output_path results/transnomer-1b-$STEP-mmlu-5shot-bf16-len256-rp1.1-xdosmaple.json \
        --batch_size auto \
        --device cuda:1 \
        2>&1 | tee logs/transnomer-1b-$STEP-mmlu-5shot-bf16-len256-rp1.1-xdosmaple.log
done