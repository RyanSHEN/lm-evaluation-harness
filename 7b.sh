# mmlu

for STEP in 69250
do
    # python main.py \
    #     --model transnormer2 \
    #     --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-7b/hf/7B-$STEP,max_length=2048,dtype="bfloat16" \
    #     --tasks hendrycksTest-* \
    #     --num_fewshot 5 \
    #     --output_path results_new/transnomer-7b-$STEP-mmlu-5shot-bf16-len2048.json \
    #     --batch_size 4 \
    #     --device cuda:2 \
    #     2>&1 | tee logs/transnomer-7b-$STEP-mmlu-5shot-bf16-len2048.log
    
    python main.py \
        --model transnormer2 \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-7b/hf/7B-$STEP,max_length=2048,dtype="bfloat16" \
        --tasks Ceval-valid-* \
        --num_fewshot 5 \
        --output_path results_new/transnomer-7b-$STEP-ceval-5shot-bp16-len2048.json \
        --batch_size 4 \
        --device cuda:2 \
        2>&1 | tee logs/transnomer-7b-$STEP-ceval-5shot-bp16-len2048.log
done