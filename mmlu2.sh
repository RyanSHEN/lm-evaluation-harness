

# mmlu

for STEP in 250 500 750 1000 3000 3250 4000
 do
    python main.py \
        --model transnormer \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-7b/hf/7B-$STEP,max_length=256,repetition_penalty=1.1,dtype="bfloat16" \
        --tasks hendrycksTest-* \
        --num_fewshot 5 \
        --output_path results/transnomer-7b-$STEP-mmlu-5shot-bf16-len256-rp1.1-xdosmaple.json \
        --batch_size auto \
        --device cuda:2 \
        2>&1 | tee logs/transnomer-7b-$STEP-mmlu-5shot-bf16-len256-rp1.1-xdosmaple.log
done