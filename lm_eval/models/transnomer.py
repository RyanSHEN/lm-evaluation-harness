from typing import Optional, Union

from lm_eval.base import BaseLM
import torch
from transformers import AutoTokenizer

from .transnormerLLM.modeling_transnormer import TransnormerForCausalLM


class TransnormerLLM(BaseLM):

    def __init__(
        self,
        device="cuda",
        low_cpu_mem_usage=None,
        path=None,
        batch_size=1,
        max_batch_size=512,
        max_length=2048,
        max_gen_token=128,
        repetition_penalty=1.0,
        trust_remote_code: Optional[bool] = True,
        dtype: Optional[Union[str, torch.dtype]] = "auto",
    ):
        super().__init__()

        self._device = torch.device(device)

        self.tokenizer = AutoTokenizer.from_pretrained(
            '/cpfs01/user/shenxuyang/LLM/Models/Baichuan-7B',
            use_fast=True,
            trust_remote_code=trust_remote_code)
        self.model = TransnormerForCausalLM.from_pretrained(
            path, torch_dtype=getattr(torch, dtype)).to(self._device)

        model_vocab_size = self.model.get_input_embeddings().weight.size(0)
        tokenzier_vocab_size = len(self.tokenizer)
        print(f"Vocab of the base model: {model_vocab_size}")
        print(f"Vocab of the tokenizer: {tokenzier_vocab_size}")

        self.model.config.pad_token_id = self.model.config.eos_token_id
        self.tokenizer.pad_token_id = self.tokenizer.eos_token_id
        self.model.eval()

        if str(batch_size).startswith("auto"):
            batch_size = batch_size.split(":")
            self.batch_size_per_gpu = batch_size[0]
            self.batch_schedule = float(
                batch_size[1]) if len(batch_size) > 1 else 1
        else:
            self.batch_size_per_gpu = int(batch_size)
        self.max_batch_size = max_batch_size

        self._max_length = max_length
        self._max_gen_token = max_gen_token
        self._repetition_penalty = repetition_penalty

    @property
    def eot_token_id(self):
        # we use EOT because end of *text* is more accurate for what we're doing than end of *sentence*
        return self.tokenizer.eos_token_id

    @property
    def max_length(self):
        return self._max_length

    @property
    def max_gen_toks(self):
        return self.max_gen_token

    @property
    def batch_size(self):
        # TODO: fix multi-gpu
        return self.batch_size_per_gpu  # * gpus

    @property
    def device(self):
        return self._device

    def tok_encode(self, string: str):
        return self.tokenizer.encode(string, add_special_tokens=False)

    def tok_decode(self, tokens):
        return self.tokenizer.decode(tokens, skip_special_tokens=True)

    def _model_call(self, inps):
        """
        inps: a torch tensor of shape [batch, sequence]
        the size of sequence may vary from call to call

        returns: a torch tensor of shape [batch, sequence, vocab] with the
        logits returned from the model
        """
        with torch.no_grad():
            return self.model(inps)[0]

    def _model_generate(self, context, max_length, eos_token_id):
        generation_kwargs = {
            "do_sample": False,
            "max_new_tokens": self._max_gen_token,
            "repetition_penalty": self._repetition_penalty
        }
        if eos_token_id is not None:
            generation_kwargs['eos_token_id'] = eos_token_id
            generation_kwargs[
                'pad_token_id'] = eos_token_id  # setting eos_token_id as pad token
        return self.model.generate(context, **generation_kwargs)


# for backwards compatibility
TRANSNORMERLLM = TransnormerLLM
