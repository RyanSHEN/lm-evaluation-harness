# ceval

for STEP in 250000 200000 150000 100000 50000
do
    python main.py \
        --model transnormer2 \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-410m/hf/410m-$STEP,max_length=2048,dtype="bfloat16" \
        --tasks Ceval-valid-* \
        --num_fewshot 5 \
        --output_path results_new/transnomer-410m-$STEP-ceval-5shot-bf16-len2048.json \
        --batch_size auto \
        --device cuda:1 \
        2>&1 | tee logs/transnomer-410m-$STEP-ceval-5shot-bf16-len2048.log
done