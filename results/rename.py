import glob
import json
from os import rename

for path in sorted(glob.glob('*.json')):

    with open(path, 'r') as f:
        jobj = json.load(f)
        if len(jobj['config']['batch_sizes']) == 0:
            bs = jobj['config']['batch_size']
        else:
            bs = jobj['config']['batch_sizes'][0]

    org_name = path
    if 'bsAuto' in org_name:
        new_name = org_name.replace('bsAuto', f'bs{bs}')
    elif f'bs{bs}' in org_name:
        new_name = org_name
    else:
        new_name = org_name[:-5] + f'-bs{bs}.json'

    rename(org_name, new_name)
    # print(path, bs)
