import os
import json
import numpy as np



def print_list(arr):
    n = len(arr)
    string = arr[0]
    for i in range(1, n):
        string += f",{arr[i]}"

    print(string)

def make_table(result_dict):
    """Generate table of results."""
    headers = ["Task", "Version", "Metric", "Value", "Stderr"]
    print_list(headers)
    values = []

    acc = []
    for k, dic in result_dict["results"].items():
        version = result_dict["versions"][k]
        for m, v in dic.items():
            if m.endswith("_stderr"):
                continue
            if "norm" in m:
                continue

            if m + "_stderr" in dic:
                se = dic[m + "_stderr"]
                # values.append([k, version, m, "%.4f" % v, "±", "%.4f" % se])
                values.append([k, version, m, "%.4f" % v, "±%.4f" % se])
                acc.append(float(v))
            else:
                values.append([k, version, m, "%.4f" % v, "", ""])
                acc.append(float(v))
            k = ""
            version = ""
            print_list(values[-1])


# key = "hendrycksTest"

dir = "/mnt/qinzhen/experiments/lm-evaluation/log"
# dir = "/mnt/qinzhen/experiments/lm-evaluation/log_mask"
# dir = "/mnt/qinzhen/experiments/lm-evaluation/log"

key = "mnli"
# key = "tnews"
# key = "hendrycksTest"
# key = "ceval"

num = 0
num = 5

# key_word = "instruct"
key_word = ""

for model in os.listdir(dir):
    if key_word not in model:
        continue
    model_path = os.path.join(dir, model)
    # print(model)
    for file in os.listdir(model_path):
        if key in file:
            file_path = os.path.join(model_path, file)
            postfix = file.split(".")[-1]
            if postfix == "json":
                try:
                    if "shot" in file:
                        shot_num = int(file.split("-")[-2])
                    else:
                        shot_num = 0
                except:
                    shot_num = 0
                if shot_num != num:
                    continue
                print("")
                print(f"{model}, {shot_num}, shot")
                with open(file_path, "r") as f:
                    json_file = json.load(f)
                    # print(json_file)
                    make_table(json_file)
            # with open(file_path, "r+", encoding="utf-8") as f:
            #     for item in jsonlines.Reader(f):
            #         print(item)
            # break%