import glob
import json

import numpy as np

for path in sorted(glob.glob('transnomer-7b-69250*-*len256*-xdosmaple*'),
                   key=lambda x: int(x.split('-')[2])):
    scores = []
    with open(path, 'r') as f:
        jobj = json.load(f)
        for k, v in jobj['results'].items():
            scores.append(float(v['acc']))

    # print(scores, len(scores))
    print(path, np.mean(scores) * 100)
