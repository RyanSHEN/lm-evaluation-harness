# mmlu

for STEP in 400000
do
    python main.py \
        --model transnormer2 \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-410m/hf/410m-$STEP,max_length=256,repetition_penalty=1.1,dtype="bfloat16" \
        --tasks hendrycksTest-* \
        --num_fewshot 5 \
        --output_path results/transnomer-410m-$STEP-mmlu-5shot-bf16-len256-rp1.1-xdosmaple.json \
        --batch_size auto \
        --device cuda:1 \
        2>&1 | tee logs/transnomer-410m-$STEP-mmlu-5shot-bf16-len256-rp1.1-xdosmaple.log

    python main.py \
        --model transnormer2 \
        --model_args path=/cpfs01/user/shenxuyang/LLM/Models/transnormerLLM-410m/hf/410m-$STEP,max_length=256,repetition_penalty=1.1,dtype="bfloat16" \
        --tasks Ceval-valid-* \
        --num_fewshot 5 \
        --output_path results/transnomer-410m-$STEP-ceval-5shot-bf16-len256-rp1.1-xdosmaple.json \
        --batch_size auto \
        --device cuda:1 \
        2>&1 | tee logs/transnomer-410m-$STEP-ceval-5shot-bf16-len256-rp1.1-xdosmaple.log
done